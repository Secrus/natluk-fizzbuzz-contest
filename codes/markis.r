library(dplyr)
y <- 0:100
case_when(
    y%%3 ==0 & y%%5==0 ~ "FizzBuzz",
    y%%3 ==0 ~ "Fizz",
    y%%5 ==0 ~ "Buzz",
    TRUE ~ as.character(y)
    )
