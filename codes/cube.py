end = 100
to_replace = {"3": "Fizz", "5": "Buzz"}


def generate():
    starter = 1
    while True:
        string = ""
        for key, value in to_replace.items():
            if starter % int(key) == 0:
                string += value
        if not string:
            string = starter
        # yield string
        print(string)
        if starter == end:
            break
        starter += 1


generate()
