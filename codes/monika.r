wektor <- c(1:100)
fizzbuzz <- (wektor %% 3 == 0 & wektor %% 5 == 0)
fizz <- (wektor %% 3 == 0)
buzz <- (wektor %% 5 == 0)

df <- data.frame(numbers=wektor)

df$fizzbuzz <- ifelse(fizzbuzz == TRUE, "fizzbuzz", (ifelse(fizz == TRUE, "fizz", (ifelse(buzz == TRUE, "buzz", "")))))
df
