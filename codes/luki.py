def fizzbuzz(number_range):
    return (
        ('FizzBuzz' if num % 5 == 0 and num % 3 == 0
         else 'Buzz' if num % 5 == 0 else 'Fizz' if num % 3 == 0 else num)
    for num in number_range)


if __name__ == '__main__':
    func = fizzbuzz(range(1, 101))
    for el in func:
        print(el)
