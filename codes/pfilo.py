def fizz_buzz(x):
    if x % 5 == 0 and x % 3 == 0:
        return 'FizzBuzz'
    elif x % 3 == 0:
        return 'Fizz'
    elif x % 5 == 0:
        return 'Buzz'
    else:
        return x

if __name__ == '__main__':
    for el in map(fizz_buzz, range(1, 101)):
        print(el)
