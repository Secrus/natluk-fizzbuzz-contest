gen = ('FizzBuzz' if not i % 3 and not i % 5 else 'Fizz' if not i % 3 else 'Buzz' if not i % 5 else i for i in range(1, 101))

while True:
    try:
        print(next(gen))
    except StopIteration:
        break
