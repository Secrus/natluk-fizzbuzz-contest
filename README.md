# Rozwiązania FizzBuzz community Natluk

## Zadanie

Wypisz 100 liczb od 1 do 100. Jeśli liczba podzielna jest przez 3, w jej miejsce wypisz "Fizz", jeśli podzielna jest przez 5, wypisz w jej miejsce "Buzz". Jeśli liczba dzieli się i przez 3 i przez 5, wypisz "FizzBuzz".

Poniżej znajduje się lista przesłanych rozwiązań zadania. Kod poszczególnych uczestników znajduje sięw katalogu `codes` w tym repozytorium.

## awos

```py
gen = ('FizzBuzz' if not i % 3 and not i % 5 else 'Fizz' if not i % 3 else 'Buzz' if not i % 5 else '' for i in range(1, 101))

while True:
    try:
        print(next(gen))
    except StopIteration:
        break
```

## Secrus

```py
for i in range(1, 101):
    result = ""
    if i % 3 == 0: result += "Fizz"
    if i % 5 == 0: result += "Buzz"
    if result == "":
        result = i
    print(result)
```

## cube

```py
end = 100
to_replace = {"3": "Fizz", "5": "Buzz"}


def generate():
    starter = 1
    while True:
        string = ""
        for key, value in to_replace.items():
            if starter % int(key) == 0:
                string += value
        if not string:
            string = starter
        # yield string
        print(string)
        if starter == end:
            break
        starter += 1


generate()
```

## stachoz

```py
for i in range(1,101):
    if (i % 3 == 0) and (i % 5 == 0):
        print("FIZZ BUZZ")
    elif i % 3 == 0:
        print("FIZZ")
    elif i % 5 == 0:
        print("BUZZ")
    else:
        print(i)
```

## SebastianEKGT

```py
for i in range(1, 101):
    if i % 3 == 0:
        print("Fizz")
    elif i % 5 == 0:
        print("Buzz")
    elif i % 5 == 0 and i % 3 == 0:
        print("FizzBuzz")
    else:
        print(i)
```

## pfilo

```py
def fizz_buzz(x):
    if x % 5 == 0 and x % 3 == 0:
        return 'FizzBuzz'
    elif x % 3 == 0:
        return 'Fizz'
    elif x % 5 == 0:
        return 'Buzz'
    else:
        return x

if __name__ == '__main__':
    for el in map(fizz_buzz, range(1, 101)):
        print(el)
```

## Tharid

```py
for i in range(1,101):
    if i % 3 == 0 and i % 5 != 0:
        print('Fizz')
    elif i % 3 != 0 and i % 5 == 0:
        print('Buzz')
    elif i % 3 == 0 and i % 5 == 0:
        print('FizzBuzz')
    else:
        print(i)
```

## Agnieszka

```py
for i in range(1, 101):
  if i%15==0:
     print('FizzBuzz')
  elif i%3==0:
     print('Fizz')
  elif i%5==0:
     print('Buzz')
  else:
    print(i)
```

## PoprostuTaki

Projekt tego uczestnika można zobaczyć [TUTAJ](https://codesandbox.io/s/fizzbuzzgame-fpoie?fontsize=14&hidenavigation=1&theme=dark). Jest to rozwiązanie w JSie z warstwą wizualną HTML i CSS.

## Markis

```r
library(dplyr)
y <- 0:100
case_when(
    y%%3 ==0 & y%%5==0 ~ "FizzBuzz",
    y%%3 ==0 ~ "Fizz",
    y%%5 ==0 ~ "Buzz",
    TRUE ~ as.character(y)
    )
```

## KrzysiekDev

```py
for i in range(1,101):
    output = ''
    if (i % 3) == 0: output += 'Fizz'
    if (i % 5) == 0: output += 'Buzz'
    if not output: output = str(i)
    print(output)
```

## Andrew7374

```py
for i in range(100):
    i += 1
    if i % 3 == 0 and i % 5 == 0:
        print('FizzBuzz')
    elif i % 5 == 0:
        print('Buzz')
    elif i % 3 == 0:
        print('Fizz')
    else:
        print(i)
```

## Luki

```py
def fizzbuzz(number_range):
    return (
        ('FizzBuzz' if num % 5 == 0 and num % 3 == 0
         else 'Buzz' if num % 5 == 0 else 'Fizz' if num % 3 == 0 else num)
    for num in number_range)


if __name__ == '__main__':
    func = fizzbuzz(range(1, 101))
    for el in func:
        print(el)
```

## Darek

```py
for i in range(1, 101):
    if i % 3 == 0 and i % 5 == 0:
        print('FizzBuzz')
    elif i % 3 == 0:
        print('Fizz')
    elif i % 5 == 0:
        print('Buzz')
    else:
        print(i)
```

## Monika

```r
wektor <- c(1:100)
fizzbuzz <- (wektor %% 3 == 0 & wektor %% 5 == 0)
fizz <- (wektor %% 3 == 0)
buzz <- (wektor %% 5 == 0)

df <- data.frame(numbers=wektor)

df$fizzbuzz <- ifelse(fizzbuzz == TRUE, "fizzbuzz", (ifelse(fizz == TRUE, "fizz", (ifelse(buzz == TRUE, "buzz", "")))))
df
```

## paszti

```py
for i in range (1, 101):
    if i%3==0 and i%5==0:
        print("FizzBuzz")
    elif i%3==0:
        print("Fizz")
    elif i%5==0:
        print("Buzz")
    else:
        print(i)
```
